from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect, render
from django.views.generic import View
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib import messages
from JUGUETES.models import *


def acceder(request):
    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            nombre_usuario = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")
            usuario = authenticate(username=nombre_usuario, password=password)
            if usuario is not None:
                login(request, usuario)
                messages.success(request, f"Bienvenid@ de nuevo {nombre_usuario}")
                return redirect("juguetes")
            else:
                messages.error(request, "Los datos son incorrectos")
        else:
            messages.error(request, "Los datos son incorrectos")
    
    form = AuthenticationForm()
    categorias = Categoria.objects.all()
    lista_productos = Producto.objects.all()
    contexto = {
    "form": form,
    "productos": lista_productos,
    "categorias": categorias
    }
    return render(request, "acceder.html", contexto)


class Registro(View):
    def get(self, request):
        categorias = Categoria.objects.all()
        lista_productos = Producto.objects.all()
        form = UserCreationForm()
        return render(request, "registro.html", {
            "form": form,
            "productos": lista_productos,
            "categorias": categorias
        })
    def post(self, request):
        form = UserCreationForm(request.POST)
        if form.is_valid():
            usuario = form.save()
            nombre_usuario = form.cleaned_data.get("username")
            messages.success(request, f"Bienvenid@ a JAGUARETE_KAA {nombre_usuario}")
            login(request, usuario)
            return redirect("juguetes")
        else:
            categorias = Categoria.objects.all()
            lista_productos = Producto.objects.all()
            for msg in form.error_messages:
                messages.error(request, form.error_messages[msg])
            return render(request, "registro.html", {
                "form": form,
                "productos": lista_productos,
                "categorias": categorias
            })
            

def salir(request):
    logout(request)
    messages.success(request, f"Tu sesión se ha cerrado correctamente")
    return redirect("acceder")