from django.urls import path
from .views import Registro, salir, acceder


urlpatterns = [
    #path('', views.index, name="index"),
    path('registro/', Registro.as_view(), name="registro"),
    path('acceder/', acceder, name="acceder"),
    path('salir/', salir, name="salir"),
] 