import os
from django.db import models
from django.contrib.auth.models import User
from django.utils import tree

# Create your models here.
class Categoria(models.Model):
    nombre = models.CharField(max_length=100, null=False, unique=True, verbose_name='Nombre')
    
    def __str__(self):
        return self.nombre
        
    class Meta:
        db_table = 'categories'
        verbose_name = 'Categoría'
        verbose_name_plural = 'Categorías'
        ordering = ['id']


class Producto(models.Model):
    nombre = models.CharField(max_length=300, verbose_name='Nombre')
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE, verbose_name='Categoría')
    imagen = models.ImageField(upload_to='productos/', blank=False, verbose_name='Imagen')
    descripcion = models.TextField(max_length=1000, verbose_name='Descripción')
    precio = models.FloatField(verbose_name='Precio')

    def delete(self, *args, **kwargs):
        if os.path.isfile(self.imagen.path):
            os.remove(self.imagen.path)
        super(Producto, self).delete(*args, **kwargs)

    def __str__(self):
        return self.nombre

    class Meta:
        db_table = 'products'
        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'
        ordering = ['id']



