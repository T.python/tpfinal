#from django.http import HttpResponseRedirect
from django.db.models import Q
from typing import Text
from django.db.models.fields import TextField
from django.http import request
from django.shortcuts import render, redirect
from django.contrib import messages
from JUGUETES.models import Producto, Categoria
from django.core.paginator import Paginator
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required

#from django.urls import reverse
from .forms import FormularioProductos



def index(request):
    categorias = Categoria.objects.all()
    lista_productos = Producto.objects.all().reverse()
    queryset = request.GET.get("busqueda")
    if queryset:
        lista_productos = Producto.objects.filter(
            Q(nombre__icontains=queryset) |
            Q(descripcion__icontains=queryset)
        ).distinct()
    paginador = Paginator(lista_productos, 10)
    pagina = request.GET.get("page") or 1
    products = paginador.get_page(pagina)
    pagina_actual = int(pagina)
    paginas = range(1, products.paginator.num_pages + 1)
    contexto = {"productos": products,
                "paginas": paginas,
                "pagina_actual": pagina_actual,
                "categorias": categorias}
    return render(request, "juguetes.html", contexto)
    

#agregar producto
@login_required(login_url='/accounts/acceder')
def agregar_producto(request):
    categorias = Categoria.objects.all()
    if request.method == "POST":
        form = FormularioProductos(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            titulo = form.cleaned_data.get("nombre")
            messages.success(request, f"El producto {titulo} se ha agregado correctamente")
        else:
            for msg in form.error_messages:
                messages.error(request, form.error_messages[msg])
        
    form = FormularioProductos()
    return render(request, "agregar_producto.html", {
        "form": form,
        "categorias": categorias
    })


@login_required(login_url='/accounts/acceder')
def eliminar_producto(request, producto_id):
    try:
        producto = Producto.objects.get(pk=producto_id)
    except Producto.DoesNotExist:
        messages.error(request, "El producto que intentas eliminar no existe")
        return redirect("juguetes")
    
    producto.delete()
    messages.success(request, f"El producto {producto.nombre} ha sido eliminado!")
    return redirect("juguetes")
    


@staff_member_required(login_url='/accounts/acceder')
def editar_producto(request, producto_id):
    categories = Categoria.objects.all()
    list_products = Producto.objects.all()
    producto = Producto.objects.get(id=producto_id)
    if request.method == 'GET':
        form = FormularioProductos(instance=producto)
        contexto = {'form': form, "productos": list_products, "categorias": categories}
    else:
        form = FormularioProductos(request.POST, instance=producto)
        if form.is_valid():
            form.save()
            return redirect("juguetes")

    return render(request, 'agregar_producto.html', contexto)

def acercade(request):
    categories = Categoria.objects.all()
    list_products = Producto.objects.all()
    contexto = {"productos": list_products, "categorias": categories}
    return render(request, "acerca_de.html", contexto)

