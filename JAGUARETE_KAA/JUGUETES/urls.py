from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name="juguetes"),
    path('agregar/producto/', agregar_producto, name="agregar_producto"),
    path('eliminar/producto/<int:producto_id>', eliminar_producto, name="eliminar_producto"),
    path('editar/producto/<int:producto_id>', editar_producto, name="editar_producto"),
    path('acerca_de/', acercade, name="acercade"),
]   

