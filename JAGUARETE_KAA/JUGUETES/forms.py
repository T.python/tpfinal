from django import forms
from django.http.response import HttpResponseRedirect
#from django.conf import settings
#from django.conf.urls.static import static
#from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.shortcuts import render
from .models import Producto, Categoria


class FormularioCategorias(forms.ModelForm):
    class Meta:
        model = Categoria
        fields = ('nombre',)


class FormularioProductos(forms.ModelForm):
    class Meta:
        model = Producto
        fields = ('nombre', 'categoria', 'imagen', 'descripcion', 'precio')

