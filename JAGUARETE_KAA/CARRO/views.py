from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import render, redirect, HttpResponse
from CARRO.carro import Carro
from JUGUETES.models import Producto, Categoria


@login_required(login_url='/accounts/acceder')
def agregar_producto(request, producto_id):
    carro = Carro(request)
    produc = Producto.objects.get(id=producto_id)
    carro.agregar(producto=produc)
    return redirect("carro:carro")


@login_required(login_url='/accounts/acceder')
def eliminar_producto(request, producto_id):
    carro = Carro(request)
    produc = Producto.objects.get(id=producto_id)
    carro.remover(producto=produc)
    return redirect("carro:carro")


@login_required(login_url='/accounts/acceder')
def restar_producto(request, producto_id):
    carro = Carro(request)
    produc = Producto.objects.get(id=producto_id)
    carro.restar(producto=produc)
    return redirect("carro:carro")


@login_required(login_url='/accounts/acceder')
def vaciar_carro(request):
    carro = Carro(request)
    carro.vaciar()
    return redirect("carro:carro")


@login_required(login_url='/accounts/acceder')
def listado_carro(request):
    lista_productos = Producto.objects.all()
    categorias = Categoria.objects.all()
    queryset = request.GET.get("busqueda")
    if queryset:
        lista_productos = Producto.objects.filter(
            Q(nombre__icontains=queryset) |
            Q(descripcion__icontains=queryset)
        ).distinct()
    return render(request, "carro.html", {"productos": lista_productos, "categorias": categorias})
