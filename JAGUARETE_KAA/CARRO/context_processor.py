from JUGUETES.models import Categoria, Producto



def monto(request):
    total = 0.0
    if request.user.is_authenticated:
        if 'carro' in request.session:
            for key, value in request.session['carro'].items():
                total = total + (float(value['precio']))
    return {'monto': total}


def cantidad_pruductos(request):
    total = 0.0
    if request.user.is_authenticated:
        if 'carro' in request.session:
            for key, value in request.session['carro'].items():
                total = total + (float(value["cantidad"]))
    return {"cantidad_pruductos": total}