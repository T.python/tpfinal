from django.http import HttpResponseRedirect
from django.shortcuts import render
from django import forms
from django.urls import reverse
#from .models import CATEGORIAS

#def index(request):
    #return render(request,"categorias/index.html",{
        #"lista_categorias": CATEGORIAS.object.all()
    #})

#class FormCategoriaCustom(forms.ModelForm):
    #campo del modelo
    #class Meta:
        #fields = ('juguetes','creatividad','vehiculos','muñecos','descripcion')

class FormAltaCategoria(forms.Form):
    CATEGORIAS = forms.CharField(label="Nueva categoria")

# Create your views here.
def index(request):
    if "categorias" not in request.session:
        request.session["categorias"] = []
    return render(request, "categorias/index.html", {
        'categorias': request.session["categorias"]
        })

def agregar(request):
    if request.method == "POST":
        form = FormAltaCategoria(request.POST)
        if form.is_valid():
            categoria = form.cleaned_data["categoria"]
            request.session["categorias"] += [categoria]
            return HttpResponseRedirect(reverse("CATEGORIAS:index"))
        else:
            return render(request, "categorias/agregar.html", {
                "formulario_alta_categoria": form
            })
    else:
        return render(request, "categoria/agregar.html", {
            "formulario_alta_categoria":FormAltaCategoria()
        })