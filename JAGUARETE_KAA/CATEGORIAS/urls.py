from django.urls import path
from . import views

app_name="CATEGORIAS"
urlpatterns = [
    path('', views.index, name="index"),
    path('agregar', views.agregar, name="agregar")
]